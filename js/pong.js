let keysDown = {};

addEventListener("keydown", function (e) {
  keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
  delete keysDown[e.keyCode];
}, false);

/**
 * Return -1 or 1 based on user input
 * @param {string} axis 
 */
function GetAxis(axis) {
  switch (axis) {
    case "Horizontal":
      if (37 in keysDown || 65 in keysDown) {
        return -1;
      } else if (39 in keysDown || 68 in keysDown) {
        return 1;
      }
    case "Vertical":
      if (38 in keysDown || 87 in keysDown) {
        return -1;
      } else if (40 in keysDown || 83 in keysDown) {
        return 1;
      }
    default:
      return 0;
  }
}

/**
 * Basic 2-dimensional vector.
 * @param {number} x 
 * @param {number} y 
 */
function Vector2(x, y) {
  this.x = x;
  this.y = y;
  this.add = (other) => new Vector2(this.x + other.x, this.y + other.y);
  this.multiply = (n) => new Vector2(this.x * n, this.y * n);
  this.divide = (n) => new Vector2(this.x / n, this.y / n);
  this.equals = (other) => this.x === other.x && this.y === other.y;
  this.toString = () => "<" + this.x + ", " + this.y + ">";
  this.magnitude = () => Math.sqrt(this.x * this.x + this.y * this.y);
  this.normalized = () => this.divide(this.magnitude());
}

/**
 * Create a HTML5 canvas.
 * @param {number} width 
 * @param {number} height 
 * @param {string} backgroundColor 
 */
function createCanvas(width = 500, height = 500, backgroundColor = 'black') {
  let canvas = document.createElement("canvas");
  canvas.width = width;
  canvas.height = height;
  canvas.ctx = canvas.getContext("2d");
  canvas.drawBackground = (color = backgroundColor) => {
    canvas.ctx.fillStyle = color;
    canvas.ctx.fillRect(0, 0, canvas.width, canvas.height);
  };
  canvas.drawBackground("black");
  return canvas;
}

/**
 * Basic GameObject.
 * @param {Vector2} position 
 * @param {Vector2} scale 
 * @param {Vector2} velocity 
 * @param {string} color 
 */
function GameObject(position = new Vector2(0, 0), scale = new Vector2(0, 0), velocity = new Vector2(0, 0), speed = 5, color = 'white') {
  this.position = position;
  this.scale = scale;
  this.velocity = velocity;
  this.speed = speed;
  this.color = color;
  this.draw = (ctx) => {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.position.x, this.position.y, this.scale.x, this.scale.y);
  };
  this.move = () => {
    this.position = this.position.add(this.velocity.multiply(this.speed));
  };
  this.isOutOfBoundsX = (canvas) => this.position.x < 0 || this.position.x + this.scale.x > canvas.width;;
  this.isOutOfBoundsY = (canvas) => this.position.y < 0 || this.position.y + this.scale.y > canvas.height;
  this.isOutOfBounds = (canvas) => this.isOutOfBoundsX(canvas) || this.isOutOfBoundsY(canvas);
  this.stayInBounds = (canvas) => {
    if (this.isOutOfBounds(canvas)) {
      this.position = this.position.add(this.velocity.multiply(-this.speed));
    }
  };
  this.collidingX = (other) => this.position.x < other.position.x + other.scale.x && this.position.x + this.scale.x > other.position.x;
  this.collidingY = (other) => this.position.y < other.position.y + other.scale.y && this.position.y + this.scale.y > other.position.y;
  this.colliding = (other) => this.collidingX(other) && this.collidingY(other);
  this.bounceX = () => {
    this.velocity = new Vector2(-this.velocity.x, this.velocity.y);
  };
  this.bounceY = () => {
    this.velocity = new Vector2(this.velocity.x, -this.velocity.y);
  };
  this.update = () => 0;
}


const pongGame = {
  "main": function () {
    this.start();
    this.update();
  },
  "start": function () {
    this.canvas = createCanvas();
    document.body.appendChild(this.canvas);

    let paddle1, paddle2, ball, p1ScoreText, p2ScoreText;

    // Paddle properties
    const paddleWidth = 5;
    const paddleHeight = 60;
    let initPosY = this.canvas.height / 2 - paddleHeight / 2;

    // Human Paddle
    let initPosX1 = 10;
    paddle1 = new GameObject(new Vector2(initPosX1, initPosY), new Vector2(paddleWidth, paddleHeight));
    paddle1.score = 0;
    paddle1.update = () => {
      paddle1.velocity = new Vector2(0, GetAxis("Vertical"));
    };

    // AI Paddle
    let initPosX2 = this.canvas.width - paddleWidth - 10;
    paddle2 = new GameObject(new Vector2(initPosX2, initPosY), new Vector2(paddleWidth, paddleHeight));
    paddle2.score = 0;
    paddle2.speed *= 0.7;
    paddle2.reactionDistance = this.canvas.width;
    paddle2.update = () => {
      // Keep paddle2 in screen
      paddle2.position.x = this.canvas.width - paddleWidth - 10;

      // Set direction towards the ball
      if (ball.position.x > this.canvas.width - paddle2.reactionDistance && ball.velocity.x > 0) {
        if ((paddle2.position.y < ball.position.y && ball.velocity.y > 0) || (paddle2.position.y > ball.position.y && ball.velocity.y < 0)) {
          paddle2.velocity = new Vector2(0, ball.velocity.y).normalized();
        } else {
          paddle2.velocity = new Vector2(0, -ball.velocity.y).normalized();
        }
      } else {
        paddle2.velocity = new Vector2(0, 0);

        // Randomize AI's reaction distance after each hit
        paddle2.reactionDistance = Math.random() * (this.canvas.width - 600) + 200;
      }
    };

    // Ball
    const ballScale = new Vector2(10, 10);
    let ballPosition = new Vector2(this.canvas.width / 2 - ballScale.x / 2, this.canvas.height / 2 - ballScale.y / 2);
    ball = new GameObject(ballPosition, ballScale);
    ball.reset = () => {
      ball.speed = 5;

      // Direction of the ball
      const dirX = dirY = [-1, 1];

      // Update Score
      if (ball.position.x < 0) {
        paddle2.score++;
        console.log("P1: " + paddle1.score + " P2: " + paddle2.score);
      } else if (ball.position.x >= this.canvas.width - ball.scale.x) {
        paddle1.score++;
        console.log("P1: " + paddle1.score + " P2: " + paddle2.score);
      }

      // Reset ball position
      ball.position =  new Vector2(this.canvas.width / 2 - ballScale.x / 2, Math.random() * (this.canvas.height - ball.scale.y));

      // Randomize ball velocity
      ball.velocity = new Vector2(dirX[Math.round(Math.random() * (dirX.length - 1))], dirY[Math.round(Math.random() * (dirY.length - 1))]);
    }
    ball.reset();
    ball.stayInBounds = (canvas) => {
      if (ball.isOutOfBoundsX(canvas)) {
        ball.reset();
      } else if (ball.isOutOfBoundsY(canvas)) {
        ball.bounceY();
      }
    };
    ball.update = () => {
      if (ball.colliding(paddle1)) {
        // Make the game harder each hit
        ball.speed *= 1.05;

        ball.velocity = new Vector2(-ball.velocity.x, (paddle1.position.y - ball.position.y) / paddle1.scale.y).normalized();
      } else if (ball.colliding(paddle2)) {
        // Make the game harder each hit
        ball.speed *= 1.05;

        ball.velocity = new Vector2(-ball.velocity.x, (paddle2.position.y - ball.position.y) / paddle2.scale.y).normalized();
      }
    };

    const scoreY = 50;

    p1ScoreText = new GameObject(new Vector2(this.canvas.width / 2 - 50, scoreY), new Vector2(50, 50));
    p1ScoreText.draw = (ctx) => {
      ctx.font = '24pt Arial';
      ctx.fillStyle = p1ScoreText.color;
      ctx.fillText(paddle1.score, this.canvas.width / 2 - 50, p1ScoreText.position.y, p1ScoreText.scale.x);
    };
    
    p2ScoreText = new GameObject(new Vector2(this.canvas.width / 2 + 50, scoreY), new Vector2(50, 50));
    p2ScoreText.draw = (ctx) => {
      ctx.font = '24pt Arial';
      ctx.fillStyle = p2ScoreText.color;
      ctx.fillText(paddle2.score, this.canvas.width / 2 + 50, p2ScoreText.position.y, p2ScoreText.scale.x);
    };

    this.gameObjects.push(paddle1);
    this.gameObjects.push(paddle2);
    this.gameObjects.push(ball);
    this.gameObjects.push(p1ScoreText);
    this.gameObjects.push(p2ScoreText);
  },
  "update": function () {
    window.requestAnimationFrame(() => this.update());
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.canvas.drawBackground();
    for (let i = 0; i < this.gameObjects.length; i++) {
      const gameObj = this.gameObjects[i];
      gameObj.draw(this.canvas.ctx);
      gameObj.update();
      gameObj.move();
      gameObj.stayInBounds(this.canvas);

      // Keep game objects in screen
      if (gameObj.position.y + gameObj.scale.y > this.canvas.height + 10) {
        gameObj.position.y = this.canvas.height - 10 - gameObj.scale.y;
      }
      if (gameObj.position.y < -10) {
        gameObj.position.y = 50;
      }
    }
  },
  "gameObjects": [],
}